package ru.chelnokov.task;

import java.io.*;
import java.util.*;

/**
 * Класс, выводящий элементы исходного массива сортированными по частоте повторения
 * Из исходного массива класс считает количество повторения каждого уникального числа,
 * затем, в соответствии с ним, создаёт двойной массив, во второй строке которого
 * от максимума к минимуму расположены количества повторений того или иного числа,
 * а в первом числа, которые привязаны к этим количествам соответственно.
 * <p>
 * Далее класс выводит в одну строку каждое число столько раз, сколько оно повторяется
 * -соре за сырые комментарии-
 *
 * @author Chelnokov E.I., 16IT18K
 */

public class RealTask {
    public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\ru\\chelnokov\\files\\input.txt"))) {
            String inputString = bufferedReader.readLine();
            String[] stringArr = inputString.split(" ");
            int[] inputArr = new int[stringArr.length];
            for (int i = 0; i < inputArr.length; i++) {
                inputArr[i] = Integer.parseInt(stringArr[i]);
            }
            LinkedHashSet<Integer> uniqueInput = new LinkedHashSet<>();
            //создаём LinkedHashSet, чтобы выделить из этих чисел уникальные
            for (int anInputArr : inputArr) {
                uniqueInput.add(anInputArr);
            }
            ArrayList<Integer> uniqueNumbers = new ArrayList<>(uniqueInput);//ArrayList с уникальными числами
            ArrayList<Integer> repetitions = repetitionsCount(inputArr, uniqueNumbers);
            //ArrayList с подсчитанными повторениями для каждого уникального числа

            int[][] sortedArray = sort(uniqueNumbers, repetitions);// массив с отсортированными данными

            print(repetitions, sortedArray);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * подсчитывает количество повторений для каждого числа
     *
     * @param inputArr      исходный массив. В нём мы будем находить повторяющиеся числа
     * @param uniqueNumbers ArrayList с уникальными числами. По нему мы будем сравнивать числа для поиска повторений
     * @return ArrayList, в котором будут соответственно записаны повторения для каждого уникального числа
     */

    private static ArrayList<Integer> repetitionsCount(int[] inputArr, ArrayList<Integer> uniqueNumbers) {
        ArrayList<Integer> repetitions = new ArrayList<>();
        int counter = 0;
        for (Integer uniqueNumber : uniqueNumbers) {
            for (int anInputArr : inputArr) {
                if (uniqueNumber == anInputArr) {
                    counter++;
                }
            }
            repetitions.add(counter);
            counter = 0;
        }
        return repetitions;
    }

    /**
     * Записывает в массив последовательно от числа с большим до числа с наименьшим количеством повторений
     *
     * @param uniqueNumbers ArrayList с уникальными числами
     * @param repetitions   ArrayList с записанным количеством повторений для каждого уникального числа
     * @return массив с отсортированными данными
     */
    private static int[][] sort(ArrayList<Integer> uniqueNumbers, ArrayList<Integer> repetitions) {
        int[][] sortedArray = new int[2][uniqueNumbers.size()];
        int a = 0;
        int max_i = 0;
        for (int i = 0; i < repetitions.size(); i++) {
            for (int j = 0; j < repetitions.size(); j++) {
                if (repetitions.get(j) > a) {
                    a = repetitions.get(j);
                    max_i = j;
                }
            }

            sortedArray[1][i] = a;
            sortedArray[0][i] = uniqueNumbers.get(max_i);
            uniqueNumbers.set(max_i, 0);
            repetitions.set(max_i, 0);
            a = 0;
            max_i = 0;
        }
        return sortedArray;
    }

    /**
     * Выводит результат и записываевает его в файл в виде, подобном записи в исходном файле
     *
     * @param repetitions ArrayList с записанным количеством повторения каждого числа.
     *                    От него нам нужно только количество элементов
     * @param sortedArray Отсортированный массив, данные которого будут выведены
     */
    private static void print(ArrayList<Integer> repetitions, int[][] sortedArray) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src\\ru\\chelnokov\\files\\output.txt"))) {

            ArrayList<Integer> outputArr = new ArrayList<>();
            for (int i = 0; i < repetitions.size(); i++) {
                for (int j = 0; j < sortedArray[1][i]; j++) {
                    outputArr.add(sortedArray[0][i]);
                    bufferedWriter.write(String.valueOf(sortedArray[0][i]) + " ");
                }
            }
            System.out.println(outputArr);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
} ///////////////////////////////////////////////П А М А Г И Т Е////////////////////////////////////////////////////////

///////////////####/////#///////#////##///////#////////#////#/#//#//###/////#////#////#/#///#/####//////////////////////
//////////////#////////#/#/////#/#///#/#/////#/#////////#//#//##////#//#//#///#//#///##/#///#/#/////////////////////////
//////////////#///////#///#///#####//###////#####////////#////##////###///#####//#//#/#/#####/#####/////////////////////
//////////////#///////#///#///#///#//#///#//#///#///////#/////#//#//#/////#///#//#/#//#/#///#/#/////////////////////////
///////////////#####/##////#/#/////#/####//#/////#/////#//////#///#/#////#/////#/#////#/#///#/#####/////////////////////
package ru.chelnokov.task;


import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;

public class FailTask {
    public static void main(String[] args) {
        int[] inputArr = {2, 5, 2, 8, 5, 6, 8, 8};
        Map<Integer, Integer> numbers = new LinkedHashMap<Integer, Integer>();
        LinkedHashSet<Integer> set = new LinkedHashSet<>();
        for (int i = 0; i < inputArr.length; i++) {
            numbers.put(inputArr[i], 0);
            set.add(inputArr[i]);
        }
        ArrayList<Integer> array = new ArrayList<>(set);
        int counter = 0;
        for (int i = 0; i < array.size(); i++) {
            for (int j = 0; j < inputArr.length; j++) {
                if (array.get(i) == inputArr[j]) {
                    counter++;
                }
            }
            numbers.put(array.get(i),counter);
            counter = 0;
        }
        System.out.println(numbers);
    }
}
